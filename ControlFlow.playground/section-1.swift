// Playground - noun: a place where people can play
/**
* author:edvard
*
* Loop and control
**/

import UIKit

for i in -99...99
{
    i*i;
}

let coursesArray = ["A","B","C"];
for item in coursesArray
{
    println(item);
}

let coursesDictionary = [1:"A",2:"B",3:"C"];

for (index,name) in coursesDictionary
{
    println(index);
    println(name);
}

for var i = -100; i <= 100; i++
{
        i*i;
}


/*
* 在swift支持多种数据类型的判断
* switch满足了一个条件后，便会自动break
* switch必须满足所有变量包含的情况
*/

let rating = "a";

//将没有包含所有情况的时候，default是不能够省略的
switch rating
    {
case "a","A":
    println("Great");
case "B":
    println("Just so-so");
default:
    println("It's bad");
}

let imString:String = "I'm a String";
switch imString
{
    case "I'm a String":
        println("I'm a String");
default:
    println("I'm readlly a String");
}

//switch语句的高级用法

/* 不知道为什么闭区间报错

var score = 90;

switch score
    {
case 0:
    println("You got an agg");
case 1..60:
    println("Sorry you failed");
case 60:
    println("Just passed");
case 61..70:
    println("Just so-so");
case 70..80:
    println("Not bad");
default:
    println("Something wrong");
}
*/

//使用下划线（_）忽略部分值
let loginResult = (true,"edvard");
let (isLoginResult,_) = loginResult;
if isLoginResult
{
    println("登录成功");
}

let coordinate = (1,1);
switch coordinate
{
case (0,0):
    println("it's at origin!");
case (_,0):
    println("(\(coordinate.0),0) is on the x-axis");
case (0,_):
    println("(0,\(coordinate.1)) is on the x-axis");
case (-2...2,-2...2):
    println("it's near the origin");
default:
    println("nothing else");
}

//value bingding
let coordinate2 = (0,2);
switch coordinate2
{
case(let x ,0):
    println("The x value is \(x)");
default:
    println("nothing!");
}

//where
let coordinate3 = (3,3);
switch coordinate3
{
case let(x,y) where x == y:
    println("x,y is on the line x = y");
default:
    println("nothing");
}

let courseInfo = ("3-2","区间运算符");
switch courseInfo
{
case (_,let courseName) where courseName.hasSuffix("运算符"):
    print("是介绍运算符的课程");
default:
    println("nothing");
}

//如果要让程序执行完case后不跳出的话，可以使用fallthrough

let coordinate4 = (0,0)

switch coordinate4
{
case (0,0):
    println("it's at origin");
    fallthrough
case (_,0):
    println("it is on the x-axis");
default:
    println("nothing");
}



