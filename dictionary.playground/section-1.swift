// Playground - noun: a place where people can play
/**
* author: edvard
* time: 2014813
**/

import Cocoa

//数组基本操作
var websites:Dictionary<String,String> = ["搜索引擎":"Google","学习网站":"w3c"];

websites.count;
websites.isEmpty;

websites["搜索引擎"];
//当字典所以没有内容时，则会返回optional值的nil
websites[";a;a;"];

//给字典添加新值，修改，等
websites["只学有用的"] = "swift学习";
//返回值为旧的值
var oldValue = websites.updateValue("百度搜索", forKey: "搜索引起");
//删除一个数据
websites["搜索引擎"] = nil;
websites.removeValueForKey("搜索引擎");

for key in websites.keys
{
    println(key);
}

for value in websites.values
{
    println(value);
}

Array(websites.keys);


//数组和字典的可变性
let dict = [1:"A",2:"B"];
//dict[1]="C";赋值 错误
//dict[3]="D";添加 错误
//dict[1]=nil;置空 错误

let array:Array = ["A","B","C"];

