// Playground - noun: a place where people can play

import Cocoa

//引入则可以使用以前oc的东西
import Foundation

var str = "Hello, playground";

/**
* 基本变量
**/

//声明常量
let MAXNUMBER = 100;

//声明变量
var index = 0;

var x = 0.0 , y = 0.5;

//具体声明变量类型

var websiteName:String = "webName";


let decimalInt:Int = 17;
let binaryInt:Int = 0b10001;
let octalInt:Int = 0o21;

//科学计数法

let bignum_a = 1000000;
let bignum_b = 1_000_000;

//强制类型转换

let num_b:Int = Int(1.2);

//可以中文命名

var 变量 = "我操";

变量 + "你好"

/**
* 布尔类型及if语句
**/

//true和false的开头都是小写
let imtrueValue = true;
let imfalse = false;

if imtrueValue
{
    println("只有一句话也需要花括号");
}
else if 3 + 4 == 7
{
    println("3 + 7 == 7");
}

/**
* 元组Tuples
**/

let registrationResult = ( true , "edvard" , "男");
let connectionResult = ( 404 , "Not Found" );

registrationResult.0;
registrationResult.1;
registrationResult.2;

let registrationResult2 = ( isRegistrationSuccess:true , nickname:"edvard" , gender:"男" );

registrationResult2.gender;
registrationResult2.nickname;

//使用下划线(_)忽略部分数值

let loginResult = ( true , "edvard" );
let ( isLoginSuccess , _ ) = loginResult;
if isLoginSuccess
{
    println("登录成功");
}

/**
* Optionals 可选值
**/

//声明为可选值变量，可以不用进行初始化，默认为nil
var imOptionalVariable:Int?

imOptionalVariable = 12;

let userInput = "abc";
//返回可选型
var age = userInput.toInt();
if age
{
    //将可选型解包为真正的int
    println("your age is " + String(age!));
}
else
{
    println("invalid userInput");
}

//循环操作(闭区间)
for index in 1...10
{
    index
}

//半闭区间
for index1 in 1...10
{
    index1
}

/**
* 字符串操作
**/

//声明空字符串
var str3 = "abc";
var str4:NSString = "你好";
var str2 = String();

//打印每一个字符串
for character in str3
{
    println(character);
}

//只能表示一个字符,不能赋值给str
var c:Character = "a";
countElements(str3);

//字符串比较
let str_a = "abc";
let str_b = "abc";

str_a == str_b;
str_a > str_b;

//字体转换成大写
str_a.uppercaseString;

//这是个数组
let chapterNames = [
    "第a一个",
    "第a二个",
    "第a三个"
];

var count = 0;
for name in chapterNames
{
    //前缀
    if name.hasPrefix("第"){
        count ++;
    }
    //后缀
    if name.hasSuffix("个"){
        count ++;
    }
}
count ;

//trim
var trimStr = "    hi    ";
//去除前面的空格
trimStr.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());

//splite
var spliteStr = "Welco-m-e to! swift";
//只按照空格拆分
spliteStr.componentsSeparatedByString(" ");

//按照空格感叹号和横岗来区分
spliteStr.componentsSeparatedByString(NSCharacterSet(charactersInString:" !-"));

/**
* 数组和字典
**/

//声明的三种方式
var array = ["A","B","C"];
var array2:[String] = ["A","B","C"];
var array3:Array<String> = ["A","B","C"];
//声明一个空数组
var array4 = [Int]();

//清空一个数组
array = [];
array = Array<String>();
//赋值
array[3] = "F";
array.append("D");

//修改0到2的数据
array[0...2] = ["E","F","G"];

//数组数量和是否为空
array.count;
array.isEmpty;

//插入在下标为0的前面
array.insert("Hello",atIndex:0);
//删除索引值为0元素
array.removeAtIndex(0);
//删除最后一个值
array.removeLast();


for index in 0...array.count
{
    println(array[index]);
}

for item in array
{
    println(item);
}

//遍历数组，取出index和值
for (index,item) in enumerate(array)
{
    println("\(index) - \(item)");
}

//字典的实例化
var websites = ["搜索引擎":"Google","学习网站":"百度百度","新闻网站":"网易新闻"];
var websites2:Dictionary<String,String> = ["搜索引擎":"Google","学习网站":"百度百度","新闻网站":"网易新闻"];

//清空字典
websites = Dictionary<String,String>();
websites = [:];




