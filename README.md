#swift-study

>edvard学习swift的路程


HelloSwift.playground


1、基本变量声明

2、类型强制转换

3、元组初始化和基本操作

4、Optional可选值

5、开闭区间

6、基本字符串操作

dictionary.playground

1、数组和字典的增删


FirstKnowUIKit.playground


1、数组和循环操作

2、UIView，UILabel做基本页面操作


ControlFlow.playground


1.switch控制基本用法

	-与其他语言的区别
2、switch控制高级用法


ControlTransfer.playground


1、二维数组的使用

2、break，fallthroug，contiue

3、跳出指定循环块


function.playground



1、带形参的函数示例

2、返回值为元组的函数

3、外部形参名称

4、可变参数的函数

5、局部可变的形参

6、inout形参

7、使用函数类型作为形参

8、返回函数类型

9、嵌套函数